
# Fidel
File Derivation Server written in golang.

![Icon of Fidel](https://raw.githubusercontent.com/michaelkrnac/fidel/master/doc/fidel.png)

# Introduction
Fidel can derivate any file into other formats by using localy installed binaries.

The files which can be derivated can be stored in the local filesystem (local assets).

Or the files can be redrived via http (remote assets).

All files are served via http server.

# Installation
Run the following commands to install fidel.

```
go get github.com/michaelkrnac/fide
cd $GOPATH/src/github.com/michaelkrnac/fide
go install
```

# Configuration
Fidel will look in the following paths and order for the configuration file "fidel.toml"

* /etc/fidel/
* $HOME/.fidel
* Current Path
* Config path defined by CLI Flag -config

Create a configuration file named fidel.toml (you can start with https://github.com/michaelkrnac/fidel/blob/master/fidel.toml)

* Define a port
* Define the four folders where to store assets
* Define fides

Fides must be defined in the follwing manner:
````
[fides]
  	[fides.<NAME>.<DERIVATIVEEXTENSION>]
		<ASSETEXTENSION> = "<BINARY NAME OR PATH> {{.Asset}} -resize 16x16 {{.Derivative}}"

````

The following example can derivate PNG, JPG, JPEG, SVG images into a PNG Image with the sizes 16x16 Pixels.

To perform the derivation Image Magic is used.

* {{.Asset}} gets filled by fidel with the filepath to the asset
* {{.Derivative}} gets filles by fidel with the filepath to the derivative

````
[fides]
  	[fides.16x16.png]
  		png = "C:/home/mkrnac/PortableApps/ImageMagick68/convert.exe {{.Asset}} -resize 16x16 {{.Derivative}}"
		jpg = "C:/home/mkrnac/PortableApps/ImageMagick68/convert.exe {{.Asset}} -resize 16x16 {{.Derivative}}"
		jpeg = "C:/home/mkrnac/PortableApps/ImageMagick68/convert.exe {{.Asset}} -resize 16x16 {{.Derivative}}"
		svg = "C:/home/mkrnac/PortableApps/rsvg/rsvg-convert.exe {{.Asset}} -h 16 -w 16 -a -f png -o {{.Derivative}}"
````

The example above explains how to derivate images into a PNG with the size of 16x16 pixels.

# Usage
## Start
To start the fidel http server run the following command
````
fidel start
````
## Local Asset
Local Assets are just files stored in the localassets folder which can be delivered.

Store a file in your localassetsfolder e.g.
````
fidels/localassets/my/cute/cat.jpg
````

You can deliver this image by calling the following URL from your Browser:
````
localhost:8080/la?p=my/cute/cat.jpg
````

## Local Derivative
Local Derivatives are local assets manipulated by a fide rule into a new file.

Lets create a thumbnail from the cat.jpg
You can deliver the thumbnail using the 16x16.png fide defined in our config.

Just point your browser to the following URL:
````
localhost:8080/ld?p=my/cute/cat.jpg&f=16x16.png
````

Fidel will derivate the image and stores it under
````
fidels/localderivatives/my/cute/cat.16x16.png
````

## Remote Asset
Remote Assets are files which can be accessed via http.
The Remote Asset is cached by fide an delivered.

Point your browser to the following URL to clone the remote asset:
````
localhost:8080/ra?u=https://raw.githubusercontent.com/michaelkrnac/fidel/master/doc/fidel.png
````

Fidel will clone the image and stores it under
````
fidels/remoteassets/raw.githubusercontent.com/michaelkrnac/fidel/master/doc/fidel.png
````

## Remote Derivative
Remote Derivatives are remote assets manipulated by a fide rule.

To create a thumbnail of our mascot point your browser to
````
localhost:8080/rd?u=https://raw.githubusercontent.com/michaelkrnac/fidel/master/doc/fidel.png&f=16x16.png
````

Fidel will derivate the image and stores it under
````
fidels/remotederivatives/raw.githubusercontent.com/michaelkrnac/fidel/master/doc/fidel.16x16.png
````

# Icon
* Gopher by Takuya Ueda (https://twitter.com/tenntenn). Licensed under the Creative Commons 3.0 Attributions license.
* Fidel by gnoki (https://www.patreon.com/gnokii). Licensed under the Public Domain
