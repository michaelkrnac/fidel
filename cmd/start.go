package cmd

import (
	"gitlab.com/michaelkrnac/fidel/server"

	"github.com/spf13/cobra"
)

var startCmd = &cobra.Command{
	Use:   "start",
	Short: "start the http server",
	Long:  "start the http server to deliver assets and derivatives",
	Run: func(cmd *cobra.Command, args []string) {
		server.Start()
	},
}
