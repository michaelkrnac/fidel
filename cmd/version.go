package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "version prints the version number",
	Long:  "version prints the version number of fidel",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("fidel version 0.0.1")
	},
}
