// Package cmd collects all fidel CLI commands
package cmd

import (
	"os"

	log "github.com/Sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// init cobra and add the global flags and commands
func init() {
	RootCmd.PersistentFlags().StringVarP(&configPath, "config", "c", "", "Load configuration fidel.toml from <`CONFIGFOLDER`>")
	RootCmd.AddCommand(versionCmd)
	RootCmd.AddCommand(startCmd)
	cobra.OnInitialize(initFidel)
}

// RootCmd is the base command of fidel
var RootCmd = &cobra.Command{
	Use:   "fidel",
	Short: "File Derivation Service",
	Long:  "File Derivation Service that just works",
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}

var configPath string

// initFidel loads the configuration file from the specified path
//
// set the config variables to default settings
// search for a config in the specified folders
// load config
// set log target
// set log level
// set log format
// set proxy
func initFidel() {
	log.Info("load default configuration")
	viper.SetDefault("server.port", 8080)
	viper.SetDefault("client.proxyurl", "")
	viper.SetDefault("client.noproxy", "")
	viper.SetDefault("client.timeout", 10)
	viper.SetDefault("logging.level", "error")
	viper.SetDefault("logging.format", "text")
	viper.SetDefault("logging.target", "console")
	viper.SetDefault("logging.file", "")
	viper.SetDefault("fide.cmdtimout", 5)     // 5 Seconds
	viper.SetDefault("fide.cachetime", 86400) // 86400 Seconds = one day
	viper.SetDefault("folders.localassets", "fidels/localassets")
	viper.SetDefault("folders.localderivatives", "fidels/localderivates")
	viper.SetDefault("folders.remoteassets", "fidels/remoteassets")
	viper.SetDefault("folders.remotederivatives", "fidels/remotederivates")

	log.Info("search for config file fidel.toml")
	viper.SetConfigName("fidel")
	viper.AddConfigPath("/etc/fidel/")
	viper.AddConfigPath("$HOME/.fidel")
	viper.AddConfigPath(".")
	viper.AddConfigPath(configPath)

	log.Info("try to load the fidel.toml")
	err := viper.ReadInConfig()
	if err != nil {
		log.Error("error loading config", err)
	}

	switch viper.GetString("logging.target") {
	case "file":
		log.Info("loggin target 'file' specified inside the config try to load config file")
		logfile, err := os.OpenFile(viper.GetString("logging.file"), os.O_APPEND|os.O_CREATE|os.O_RDWR, 0666)
		if err != nil {
			log.Error("error opening log file: %v", err)
		}
		log.SetOutput(logfile)
	default:
	}

	log.Info("try to set logging level from config")
	logLevel, err := log.ParseLevel(viper.GetString("logging.level"))
	if err != nil {
		log.Error("error setting log level", err)
	}
	log.SetLevel(logLevel)

	switch viper.GetString("logging.format") {
	case "text":
		log.SetFormatter(&log.TextFormatter{
			FullTimestamp: true,
		})
		log.Info("logging format set to text")
	case "json":
		log.SetFormatter(&log.JSONFormatter{})
		log.Info("logging format set to json")
	default:
		log.SetFormatter(&log.TextFormatter{
			FullTimestamp: true,
		})
		log.Info("logging format set to text")
	}

	if viper.GetString("client.proxyurl") != "" {
		os.Setenv("HTTP_PROXY", viper.GetString("client.proxyurl"))
		os.Setenv("NO_PROXY", viper.GetString("client.noproxy"))
		log.Info("exported proxy environment variables set in the config")
	}
}
