package main

import (
	"os"

	log "github.com/Sirupsen/logrus"
	"gitlab.com/michaelkrnac/fidel/cmd"
)

// main
//
// executes the cobra cli root cmd
// it is the only entrypoint for fidel
func main() {
	err := cmd.RootCmd.Execute()
	if err != nil {
		log.Error(err)
		os.Exit(-1)
	}
}
