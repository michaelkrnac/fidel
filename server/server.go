// Package server starts the http server
package server

import (
	"fmt"
	"net/http"
	"strconv"

	log "github.com/Sirupsen/logrus"
	"github.com/spf13/viper"
)

// Start the http server
// to deliver local assets, local derivatives,
// remote assets, remote deirivatives
func Start() {
	http.HandleFunc("/", homeHandler)
	http.HandleFunc("/la", localassetHandler)
	http.HandleFunc("/ld", localderivativeHandler)
	http.HandleFunc("/ra", remoteassetHandler)
	http.HandleFunc("/rd", remotederivativeHandler)

	if viper.GetString("logging.target") == "file" {
		fmt.Println("logfile can be found at :", viper.GetString("logging.file"))
	}

	log.Info("start server on port :", viper.GetInt("server.port"))
	http.ListenAndServe(":"+strconv.Itoa(viper.GetInt("server.port")), nil)
}
