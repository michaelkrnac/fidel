package server

import (
	"net/http"
	"strconv"

	log "github.com/Sirupsen/logrus"
	"github.com/pkg/errors"
	"github.com/spf13/viper"
	"gitlab.com/michaelkrnac/fidel/fide"
)

// localderivateHandler handles a user request to deliver
// a local asset derived into a from the user defined format
//
// get the path to the local asset from the user inside the url p= flag
// get the name of the filederivator from the user inside the url f= flag
// create new local asset form the path
// create new fide based on the fidename
// create new local derivative based on the local asset and the fide
// deliver the local derivative with cache control
func localderivativeHandler(w http.ResponseWriter, r *http.Request) {

	path := r.URL.Query().Get("p")
	if path == "" {
		http.Error(w, "no path defined in url", 400)
		return
	}

	fideName := r.URL.Query().Get("f")
	if fideName == "" {
		http.Error(w, "no fide name defined in url", 400)
		return
	}

	la, err := fide.NewLocalAsset(path)
	if err != nil {
		log.Info(errors.Wrap(err, "local asset not found at path="+path))
		http.Error(w, "asset not found", 404)
		return
	}

	fi, err := fide.NewFide(fideName)
	if err != nil {
		http.Error(w, "fide not found", 404)
		return
	}

	ld, err := fide.Derivater(la, fi)
	if err != nil {
		log.Error(errors.Wrap(err, "error derivate the local asset"))
		http.Error(w, "error derivate the local asset", 500)
		return
	}

	log.Info("local derivative delivered path=" + ld.GetRelativeFilePath())
	w.Header().Set("Cache-Control", "max-age="+strconv.Itoa(viper.GetInt("fide.cachetime")))
	http.ServeFile(w, r, ld.GetAbsoluteFilePath())
}
