package server

import (
	"net/http"
	"strconv"

	log "github.com/Sirupsen/logrus"
	"github.com/pkg/errors"
	"github.com/spf13/viper"
	"gitlab.com/michaelkrnac/fidel/fide"
)

// localassetHandler get a user request for a local asset
// and delivers it if possible
//
// get the path from the user inside the url p= flag
// create local asset which also checks if the asset exists
// deliver the file with cache control
func localassetHandler(w http.ResponseWriter, r *http.Request) {

	path := r.URL.Query().Get("p")
	if path == "" {
		http.Error(w, "no path defined in url", 400)
		return
	}

	la, err := fide.NewLocalAsset(path)
	if err != nil {
		log.Info(errors.Wrap(err, "local asset not found at path="+path))
		http.Error(w, "local asset not found", 404)
		return
	}

	log.Info("local asset delivered path=" + la.GetRelativeFilePath())
	w.Header().Set("Cache-Control", "max-age="+strconv.Itoa(viper.GetInt("fide.cachetime")))
	http.ServeFile(w, r, la.GetAbsoluteFilePath())
}
