package server

import (
	"net/http"
	"strconv"

	log "github.com/Sirupsen/logrus"
	"github.com/pkg/errors"
	"github.com/spf13/viper"
	"gitlab.com/michaelkrnac/fidel/fide"
)

// remotederivativeHandler get a user request for derivate a remote asset
//
// get the url from the user inside the url u= flag
// get the fide from the user inside the f= flag
// create new remote asset based on teh url
// create new fide based on the fide name
// create new remote derivative based on the remote asset and fide
// deliver the remote derivative with cache control
func remotederivativeHandler(w http.ResponseWriter, r *http.Request) {

	url := r.URL.Query().Get("u")
	if url == "" {
		http.Error(w, "no url defined", 400)
		return
	}

	fideName := r.URL.Query().Get("f")
	if fideName == "" {
		http.Error(w, "no fide name defined", 400)
		return
	}

	ra, err := fide.Cloner(url)
	if err != nil {
		log.Error(errors.Wrap(err, "remote asset clone error from url="+url))
		http.Error(w, "error cloning the remote asset from url", 500)
		return
	}

	fi, err := fide.NewFide(fideName)
	if err != nil {
		http.Error(w, "fide not found", 404)
		return
	}

	rd, err := fide.Derivater(ra, fi)
	if err != nil {
		log.Error(errors.Wrap(err, "remote derivative derivate error from url="+url))
		http.Error(w, "error preparing the remote derivate", 500)
		return
	}

	log.Info("remote derivative delivered path=" + rd.GetRelativeFilePath())
	w.Header().Set("Cache-Control", "max-age="+strconv.Itoa(viper.GetInt("fide.cachetime")))
	http.ServeFile(w, r, rd.GetAbsoluteFilePath())

}
