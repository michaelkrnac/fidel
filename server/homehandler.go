package server

import (
	"fmt"
	"net/http"
)

// homeHandler just sends the fidel version
func homeHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "fidel version 0.0.1")
}
