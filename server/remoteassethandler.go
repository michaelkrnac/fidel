package server

import (
	"net/http"
	"strconv"

	log "github.com/Sirupsen/logrus"
	"github.com/pkg/errors"
	"github.com/spf13/viper"
	"gitlab.com/michaelkrnac/fidel/fide"
)

// remoteassetHandler delivers remote assets
//
// get the url from the user inside the url u= flag
// create a new remote assed based on the url
// try to clone the asset via http
// deliver the remote assed with cache control
func remoteassetHandler(w http.ResponseWriter, r *http.Request) {

	url := r.URL.Query().Get("u")
	if url == "" {
		http.Error(w, "no url defined", 400)
		return
	}

	ra, err := fide.Cloner(url)
	if err != nil {
		log.Error(errors.Wrap(err, "remote asset clone error from url="+url))
		http.Error(w, "error creating remote asset from url", 500)
		return
	}

	log.Info("remote asset delivered path=" + ra.GetRelativeFilePath())
	w.Header().Set("Cache-Control", "max-age="+strconv.Itoa(viper.GetInt("fide.cachetime")))
	http.ServeFile(w, r, ra.GetAbsoluteFilePath())

}
