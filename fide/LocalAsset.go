package fide

import (
	"os"
	"path/filepath"
	"strings"

	"github.com/pkg/errors"
	"github.com/spf13/viper"
)

// LocalAsset represents a file stored in the
// local assets folder defined in the configuration.
//
// A local asset is an asset where the source of the file
// is stored in a file on this machine.
// E.g. the original format of an image
type LocalAsset struct {
	path          string
	relFolderPath string
	relFilePath   string
	absFolderPath string
	absFilePath   string
	fileName      string
	fileExt       string
}

// NewLocalAsset create new local asset from an localAssetPath.
//
// New Local Asset also checks if the Asset file exists
// and returns a local asset.
func NewLocalAsset(path string) (la LocalAsset, err error) {
	la.path = path
	la.relFolderPath = filepath.Dir(path)
	la.relFilePath = path
	la.absFolderPath = filepath.Join(viper.GetString("folders.localassets"), la.relFolderPath)
	la.absFilePath = filepath.Join(viper.GetString("folders.localassets"), la.relFilePath)
	la.fileName = strings.TrimSuffix(filepath.Base(path), filepath.Ext(path))
	la.fileExt = filepath.Ext(path)

	if _, err := os.Stat(la.absFilePath); os.IsNotExist(err) {
		return la, errors.Wrap(err, "local asset file not found")
	}
	return la, nil
}

// GetRelativeFilePath returns the relative file path to the local asset
func (la LocalAsset) GetRelativeFilePath() (path string) {
	return la.relFilePath
}

// GetAbsoluteFilePath returns the absolute file path to the local asset
func (la LocalAsset) GetAbsoluteFilePath() (path string) {
	return la.absFilePath
}

//GetAbsoluteFolderPath returns the absolute folder path to the remote asset
func (la LocalAsset) GetAbsoluteFolderPath() (path string) {
	return la.absFolderPath
}

//IsAsset renturns true because this is an asset
func (la LocalAsset) IsAsset() bool {
	return true
}
