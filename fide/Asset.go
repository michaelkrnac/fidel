package fide

// Asset represents a file
// at a predefined folder
type Asset interface {
	IsAsset() bool
	GetAbsoluteFilePath() string
}
