package fide

import (
	"io"
	"mime"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/pkg/errors"
	"github.com/spf13/viper"
)

// Cloner clones the file from the url into a remote asset
//
// create new remote asset from url
// if the remote asset does not exists clone it
// if remote asset exists clone it, if it is older
// than the defined cachetime
func Cloner(URL string) (ra RemoteAsset, err error) {
	ra, err = NewRemoteAsset(URL)
	if err != nil {
		return ra, errors.Wrap(err, "invalid url")
	}

	switch IsNotExistFolder(ra) {
	case true:
		err := clone(&ra)
		if err != nil {
			return ra, errors.Wrap(err, "could not clone")
		}
	case false:
		file, err := os.Stat(ra.absFolderPath)
		if err != nil {
			return ra, errors.Wrap(err, "error opening remote asset file")
		}
		cachetime := viper.GetDuration("fide.cachetime").Seconds()
		lastmodifiedtime := time.Since(file.ModTime()).Seconds()
		if lastmodifiedtime >= cachetime {
			err := clone(&ra)
			if err != nil {
				return ra, errors.Wrap(err, "could not clone")
			}
		}
	}
	return ra, nil
}

// clone a original file via http from url
//
// load the file via http request
// calculate file extension based on http content type
// make dir
// create file
// write response body to file
func clone(ra *RemoteAsset) error {

	var client = &http.Client{
		Timeout: viper.GetDuration("client.timeout") * time.Second,
	}
	r, err := client.Get(ra.url)
	if err != nil {
		return errors.Wrap(err, "error retriving remote asset")
	}
	defer r.Body.Close()

	if r.StatusCode == 404 {
		return errors.New("remote asset not found (HTTP 404)")
	}

	if r.StatusCode != 200 {
		return errors.New("error loading remote asset (HTTP " + strconv.Itoa(r.StatusCode) + ")")
	}

	e, err := mime.ExtensionsByType(r.Header.Get("content-type"))
	if err != nil {
		return errors.Wrap(err, "filetype not delivered")
	}
	ra.SetExt(e[0])
	ra.CalculateFilePath()

	err = os.MkdirAll(ra.GetAbsoluteFolderPath(), 0777)
	if err != nil {
		return errors.Wrap(err, "error make dir of the remote asset folder")
	}

	file, err := os.Create(ra.GetAbsoluteFilePath())
	if err != nil {
		return errors.Wrap(err, "error create remote asset file")
	}
	defer file.Close()

	_, err = io.Copy(file, r.Body)
	if err != nil {
		return errors.Wrap(err, "error copy file content")
	}

	return nil
}
