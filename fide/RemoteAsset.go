package fide

import (
	"net/url"
	"path/filepath"

	"github.com/pkg/errors"
	"github.com/spf13/viper"
)

// RemoteAsset represents a clone of a remote asset.
//
// A remote asset is a file stored in the network
// and can be redrived via a http request.
// We always clone the remote asset into the
// local file system to derivate it later on
type RemoteAsset struct {
	url           string
	relFolderPath string
	relFilePath   string
	absFolderPath string
	absFilePath   string
	fileName      string
	fileExt       string
}

// NewRemoteAsset creates new remote asset from an URL.
//
// It only transforms the url into a valid filepath.
// The asset has to be cloned to make it present on the disk
// create asset filepath based on the url in the manner of host + path
func NewRemoteAsset(URL string) (ra RemoteAsset, err error) {

	u, err := url.Parse(URL)
	if err != nil {
		return ra, errors.Wrap(err, "there was an error parsing the url")
	}

	ra.url = URL
	ra.relFolderPath = filepath.Join(u.Host, u.Path)
	ra.absFolderPath = filepath.Join(viper.GetString("folders.remoteassets"), ra.relFolderPath)
	ra.fileName = "original"

	return ra, nil
}

//GetRelativeFilePath returns the realtive file path to the remote derivative
func (ra RemoteAsset) GetRelativeFilePath() (path string) {
	return ra.relFilePath
}

//GetAbsoluteFilePath returns the absolute file path to the remote asset
func (ra RemoteAsset) GetAbsoluteFilePath() (path string) {
	return ra.absFilePath
}

//GetAbsoluteFolderPath returns the absolute folder path to the remote asset
func (ra RemoteAsset) GetAbsoluteFolderPath() (path string) {
	return ra.absFolderPath
}

//GetFileName returns the path to the local derivative
func (ra RemoteAsset) GetFileName() (filename string) {
	return ra.fileName + ra.fileExt
}

//IsAsset returns true because this is an asset
func (ra RemoteAsset) IsAsset() bool {
	return true
}

//SetExt sets the extension
func (ra *RemoteAsset) SetExt(ext string) {
	ra.fileExt = ext
}

//CalculateFilePath calculates the filepath based on name and extension
func (ra *RemoteAsset) CalculateFilePath() {
	ra.relFilePath = filepath.Join(ra.relFolderPath, ra.fileName+ra.fileExt)
	ra.absFilePath = filepath.Join(viper.GetString("folders.remoteassets"), ra.relFilePath)
}
