package fide

import (
	"path/filepath"
	"strings"

	"github.com/spf13/viper"
)

// LocalDerivative is a derivative of an local asset.
//
// A local derivative is stored in the local derivative folder.
// It is stored under the same path like the local asset.
// the filename is extended by the filename + the fidename
type LocalDerivative struct {
	la            LocalAsset
	fide          Fide
	relFolderPath string
	relFilePath   string
	absFolderPath string
	absFilePath   string
	fileName      string
	fileExt       string
}

// NewLocalDerivative create a new local derivative from a local asset
//
// This function only prepares an LocalDerivative.
// To make it present on the file system the derivater has to be used.
func NewLocalDerivative(la LocalAsset, f Fide) (ld LocalDerivative) {
	ld.la = la
	ld.fide = f
	ld.fileName = strings.TrimSuffix(la.fileName+"."+f.name, filepath.Ext(f.name))
	ld.fileExt = filepath.Ext(f.name)
	ld.relFolderPath = la.relFolderPath
	ld.relFilePath = filepath.Join(ld.relFolderPath, ld.fileName+ld.fileExt)
	ld.absFolderPath = filepath.Join(viper.GetString("folders.localderivatives"), ld.relFolderPath)
	ld.absFilePath = filepath.Join(viper.GetString("folders.localderivatives"), ld.relFilePath)

	return ld
}

//GetRelativeFilePath returns the relative file path to the local derivative
func (ld LocalDerivative) GetRelativeFilePath() (path string) {
	return ld.relFilePath
}

//GetAbsoluteFilePath returns the absolute file path to the local derivative
func (ld LocalDerivative) GetAbsoluteFilePath() (path string) {
	return ld.absFilePath
}

//GetAbsoluteFolderPath returns the absolute folder path to the local derivatíve
func (ld LocalDerivative) GetAbsoluteFolderPath() (path string) {
	return ld.absFolderPath
}

//IsDerivative returns true because this is an derivative
func (ld LocalDerivative) IsDerivative() bool {
	return true
}
