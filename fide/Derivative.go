package fide

// Derivative represents a derivative of an asset.
//
// The derivative interface is fullfilled,
// when a type implements the method IsDerivative
type Derivative interface {
	IsDerivative() bool
	GetRelativeFilePath() string
	GetAbsoluteFilePath() string
	GetAbsoluteFolderPath() string
}
