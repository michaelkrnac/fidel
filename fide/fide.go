// Package fide provides everything needed to derivate assets
package fide

import (
	"github.com/pkg/errors"
	"github.com/spf13/viper"
)

// Fide represents a target file type e.g. "16x16.png".
//
// Name must be unique and it is a combination of
// an identifer and the target file extension.
//
// Cmds is a map where the key defines
// the source file extension e.g. "jpeg"
// and the value is the cmd which should be
// executed to derviate the source file
// into the target file e.g.
// execution of image magic with specific flags
type Fide struct {
	name string
	cmds map[string]string
}

// NewFide constructes new fide by fide name.
// Also checks if there is a valid configuration
// for this fide name in the configuration.
func NewFide(name string) (f Fide, err error) {
	f.name = name
	// check if fide exists in the config
	if viper.IsSet("fides."+f.name) != true {
		return f, errors.New("file derivation (fide) not found in config")
	}
	// load commands from config
	f.cmds = viper.GetStringMapString("fides." + f.name)
	return f, nil
}
