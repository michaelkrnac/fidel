package fide

import (
	"path/filepath"
	"strings"

	"github.com/spf13/viper"
)

// RemoteDerivative is a derivate of a remote asset.
//
// A file accessable via http can be cloned to an remote asset.
// The Remote asset can than be derivated into an remote derivative.
// This function only prepares a remote derivative,
// to make it present on the filesystem the derivater have to be used.
type RemoteDerivative struct {
	ra            RemoteAsset
	fide          Fide
	relFolderPath string
	relFilePath   string
	absFolderPath string
	absFilePath   string
	fileName      string
	fileExt       string
}

// NewRemoteDerivative create a new derivative from a remote asset
func NewRemoteDerivative(ra RemoteAsset, f Fide) (rd RemoteDerivative) {
	rd.ra = ra
	rd.fide = f
	rd.fileName = strings.TrimSuffix(ra.fileName+"."+f.name, filepath.Ext(f.name))
	rd.fileExt = filepath.Ext(f.name)
	rd.relFolderPath = ra.relFolderPath
	rd.relFilePath = filepath.Join(rd.relFolderPath, rd.fileName+rd.fileExt)
	rd.absFolderPath = filepath.Join(viper.GetString("folders.remotederivatives"), rd.relFolderPath)
	rd.absFilePath = filepath.Join(viper.GetString("folders.remotederivatives"), rd.relFilePath)
	return rd
}

//GetRelativeFilePath returns the path to the remote derivative
func (rd RemoteDerivative) GetRelativeFilePath() (path string) {
	return rd.relFilePath
}

//GetAbsoluteFilePath returns the absolute path to the remote derivate
func (rd RemoteDerivative) GetAbsoluteFilePath() (path string) {
	return rd.absFilePath
}

//GetAbsoluteFolderPath returns the absolute folder path to the remote derivative
func (rd RemoteDerivative) GetAbsoluteFolderPath() (path string) {
	return rd.absFolderPath
}

//IsDerivative returns true because this is an derivative
func (rd RemoteDerivative) IsDerivative() bool {
	return true
}
