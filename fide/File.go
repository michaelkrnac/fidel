package fide

import "os"

// File represeants a file on the filesystem.
//
// Every type wich can provide a absolute path
// to a file fullfills the interface File
type File interface {
	GetAbsoluteFilePath() string
	GetAbsoluteFolderPath() string
}

// Exist checks if the file behind the AbsPath exists
func Exist(f File) (b bool) {
	if _, err := os.Stat(f.GetAbsoluteFilePath()); os.IsNotExist(err) {
		return false
	}
	return true
}

// IsNotExist checks if the file behind the AbsPath not exists
func IsNotExist(f File) (b bool) {
	if _, err := os.Stat(f.GetAbsoluteFilePath()); os.IsNotExist(err) {
		return true
	}
	return false
}

// IsNotExistFolder checks if the folder behind the AbsFolderPath not exists
func IsNotExistFolder(f File) (b bool) {
	// check if the source dir exist
	src, err := os.Stat(f.GetAbsoluteFolderPath())
	if err != nil {
		return true
	}

	// check if the source is indeed a directory or not
	if !src.IsDir() {
		return true
	}
	return false
}


