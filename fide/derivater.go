package fide

import (
	"bytes"
	"context"
	"html/template"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"
	"time"

	"github.com/pkg/errors"
	"github.com/spf13/viper"
)

// Derivater derivates a derivative if necessary
//
// based on the asset type creat a new derivative object
// check if derivative allready exists
// if derivat not exists derivate it
// if derivative exists get the file info and check if the
// derivative is older than the defined cachetime and derivate iagain or return it
func Derivater(a Asset, f Fide) (d Derivative, err error) {

	switch a.(type) {
	default:
		return d, errors.New("Asset type error")
	case LocalAsset:
		d = NewLocalDerivative(a.(LocalAsset), f)
	case RemoteAsset:
		d = NewRemoteDerivative(a.(RemoteAsset), f)
	}

	switch IsNotExist(d) {
	case true:
		err := derivate(a, d, f)
		if err != nil {
			return d, errors.Wrap(err, "could not derivate")
		}
	case false:
		file, err := os.Stat(d.GetAbsoluteFilePath())
		if err != nil {
			return d, errors.Wrap(err, "open derivative error")
		}
		cachetime := viper.GetDuration("fide.cachetime").Seconds()
		lastmodifiedtime := time.Since(file.ModTime()).Seconds()
		if lastmodifiedtime >= cachetime {
			err := derivate(a, d, f)
			if err != nil {
				return d, errors.Wrap(err, "could not derivate")
			}
		}
	}

	return d, nil
}

// derivate derivates an asset based on the fide to a derivate
//
// we have to ensure that there is a command defined
// of how to come from the source asset to the target derivative
// if there is none defined return error
// we have a valid source, a valid fide with a commandtemplate
// so lets create the folder for the derivative
// create the commandline cmd based on the fide template
// execute the command
func derivate(asset Asset, derivative Derivative, f Fide) error {

	e := strings.TrimPrefix(filepath.Ext(asset.GetAbsoluteFilePath()), ".")
	cmdtpl, ok := f.cmds[e]
	if !ok {
		return errors.New("fide rule for source type not defined")
	}

	err := os.MkdirAll(derivative.GetAbsoluteFolderPath(), 0777)
	if err != nil {
		return errors.Wrap(err, "make dir of the target derivative folder failed")
	}

	// we have to generate the command based on the template
	type Cmd struct {
		Asset      string
		Derivative string
	}

	c := Cmd{
		Asset:      asset.GetAbsoluteFilePath(),
		Derivative: derivative.GetAbsoluteFilePath(),
	}

	tpl, err := template.New("cmdtpl").Parse(cmdtpl)
	if err != nil {
		return errors.Wrap(err, "error parsing fide command template")
	}

	buf := new(bytes.Buffer)
	err = tpl.Execute(buf, c)
	if err != nil {
		return errors.Wrap(err, "error filling command template")
	}
	cmd := buf.String()

	t := viper.GetDuration("fide.cmdtimeout")
	ctx, cancel := context.WithTimeout(context.Background(), t*time.Second)
	defer cancel()

	switch runtime.GOOS {
	case "windows":
		err = exec.CommandContext(ctx, "cmd", "/C", cmd).Run()
	case "linux":
		err = exec.CommandContext(ctx, cmd).Run()
	default:
		err = exec.CommandContext(ctx, cmd).Run()
	}
	if err != nil {
		return errors.Wrap(err, "error executing the file derivate command")
	}

	return nil
}
